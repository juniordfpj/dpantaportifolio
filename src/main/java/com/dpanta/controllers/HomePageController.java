package com.dpanta.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomePageController {
	
	@RequestMapping("/home")
	public String getHomePage(){
		return "home";
	}
	
	@RequestMapping("/contactme")
	public String getContactPage(){
		return "contactme";
	}
	
}
