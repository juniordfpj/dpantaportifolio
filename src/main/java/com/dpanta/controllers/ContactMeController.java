package com.dpanta.controllers;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.io.IOException;
import java.util.Locale;

import javax.mail.MessagingException;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.dpanta.services.EmailSenderService;

@Controller
@RequestMapping
public class ContactMeController {
	
	ApplicationContext context = new ClassPathXmlApplicationContext("Spring-EmailConfig.xml");

	EmailSenderService emailSenderService = (EmailSenderService) context.getBean("emailSenderService");

	@RequestMapping(value = "/sendEmail", method = POST)
    public String sendMailWithAttachment(
        @RequestParam("recipientName") final String recipientName,
        @RequestParam("recipientEmail") final String recipientEmail,
        @RequestParam("recipientEmailSubject") final String recipientEmailSubject,
        @RequestParam("recipientMessage") final String recipientMessage,
        @RequestParam("emailAttachment") final MultipartFile emailAttachment,
        final Locale locale)
        throws MessagingException, IOException {
		
		try{
			
			emailSenderService.sendAutoResponseMail(recipientName, recipientEmail);
	        
			emailSenderService.sendMailWithAttachment(recipientName, recipientEmail, recipientEmailSubject,
	        		recipientMessage, emailAttachment.getOriginalFilename(), emailAttachment.getInputStream());
			
			return "/emailSentPage";
			
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("An erro occurred while sending the emails!");
		}
		
		return "/errorPage";
    }
	
	
}
