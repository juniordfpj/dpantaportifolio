package com.dpanta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DpantaportifolioApplication {

	public static void main(String[] args) {
		SpringApplication.run(DpantaportifolioApplication.class, args);
	}
}
