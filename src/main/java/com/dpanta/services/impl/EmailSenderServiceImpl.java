package com.dpanta.services.impl;

import java.io.IOException;
import java.io.InputStream;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.dpanta.services.EmailSenderService;

public class EmailSenderServiceImpl implements EmailSenderService {

	private JavaMailSender mailSender;

	private SimpleMailMessage simpleMailMessage;

	public void setSimpleMailMessage(SimpleMailMessage simpleMailMessage) {
		this.simpleMailMessage = simpleMailMessage;
	}

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	@Override
	public void sendMailWithAttachment(String recipientName, String recipientEmail, String recipientEmailSubject,
			String recipientMessage, String originalFilename, InputStream inputStream) throws IOException {

		MimeMessage message = mailSender.createMimeMessage();

		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(simpleMailMessage.getFrom());
			helper.setTo(simpleMailMessage.getTo());
			helper.setSubject(recipientEmailSubject);
			helper.setText(recipientMessage);
			helper.addAttachment(originalFilename, new ByteArrayResource(IOUtils.toByteArray(inputStream)));

		} catch (MessagingException e) {
			System.out.println("An error occured while tryting to send your email: sendMailWithAttachment");
			e.printStackTrace();
		}
		
		mailSender.send(message);
	}

	@Override
	public void sendAutoResponseMail(String recipientName, String recipientEmail) {
		MimeMessage message = mailSender.createMimeMessage();

		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);
			helper.setFrom(simpleMailMessage.getFrom());
			helper.setTo(recipientEmail);
			helper.setSubject(simpleMailMessage.getSubject());
			helper.setText(String.format(simpleMailMessage.getText(), recipientName));

		} catch (MessagingException e) {
			System.out.println("An error occured while tryting to send your email: sendAutoResponseMail");
			e.printStackTrace();
		}
		mailSender.send(message);
	}

}
