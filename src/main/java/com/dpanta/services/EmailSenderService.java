package com.dpanta.services;

import java.io.IOException;
import java.io.InputStream;

public interface EmailSenderService {
	
	public void sendMailWithAttachment(String recipientName, String recipientEmail, String recipientEmailSubject,
			String recipientMessage, String originalFilename, InputStream inputStream) throws IOException;
	
	public void sendAutoResponseMail(String recipientName, String recipientEmail);
}
